#Sistemas Distribuídos - 2017.2
#Projeto Final de SD - Sistema de Arquivos
#Gabriel da Rocha Silva - 374927

#IMPORTANTE: FALTA COMENTAR OS CÓDIGOS!!!

import xmlrpc.client

print("Cliente")
print("Conectando ao servidor proxy.")

proxy = xmlrpc.client.ServerProxy("http://localhost:8000/")
var = True

while var:
    print("Opcoes de Arquivo")
    print("1 - Ler")
    print("2 - Criar")
    print("3 - Modificar")
    print("4 - Apagar")
    opcao = input(print("Digite opção: "))
    print("")

    if opcao == "1":
        try:
            arquivo = input(print("Digite nome: "))
            printar = proxy.ler(arquivo)
            print('\n'+printar[0])
            print(printar[1])
            print('\n')
        except IOError:
            print("Servidor proxy falhou. Encerrando programa.")
            var = False

    elif opcao == "2":
        try:
            arquivo = input(print("Digite nome: "))
            texto = input(print("Digite texto a ser escrito: "))
            retorno = proxy.criar(arquivo, texto) #Colocar um retorno aqui
            print(retorno)
        except IOError:
            print("Servidor proxy falhou. Encerrando programa.")
            var = False

    elif opcao == "3":
        try:
            arquivo = input(print("Digite nome: "))
            texto = input(print("Digite texto a ser adicionado: "))
            retorno = proxy.modificar(arquivo, texto) #Colocar um retorno aqui
            print(retorno)
        except IOError:
            print("Servidor proxy falhou. Encerrando programa.")
            var = False

    elif opcao == "4":
        try:
            arquivo = input(print("Digite nome: "))
            retorno = proxy.apagar(arquivo) #Colocar um retorno aqui
            print(retorno)
        except IOError:
            print("Servidor proxy falhou. Encerrando programa.")
            var = False

    if var:
        ent = input(print("Continuar? 1 - Sim | 2 - Não : "))
        if ent == '1':
            var = True
        elif ent == '2':
            var = False
            var2 = input(print("Quer o log de todas as operações feitas? 1 - Sim | 2 - Não\n"))
            if var2 == '1':
                retorno = proxy.envialog()
                print("")
                print("[ Operação - Nome arquivo ][ ID Datanode ][ Mensagem de resposta ]\n")
                print(retorno)
            else:
                print("Você ficou sem log.\n")
