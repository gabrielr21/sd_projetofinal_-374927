#Sistemas Distribuídos - 2017.2
#Projeto Final de SD - Sistema de Arquivos
#Gabriel da Rocha Silva - 374927

from xmlrpc.server import SimpleXMLRPCServer
import os

def le_arquivo(nome):
    nome = pasta + nome + ".txt"
    arq = open(nome,'r')
    arc = arq.read()
    arq.close()
    return str(arc)

def cria_arquivo(nome, texto):
    nome = pasta + nome + ".txt"
    arq = open(nome, 'w')
    arq.write(texto)
    arq.close()
    return "ok"

def modifica_arquivo(nome, texto):
    nome = pasta + nome + ".txt"
    arq = open(nome, 'a')
    arq.write(texto)
    arq.close()
    return "ok"

def apaga_arquivo(nome):
    nome = pasta + nome + ".txt"
    os.remove(nome)
    return "ok"

def lista_arquivo():
    listarq = os.listdir(pasta)
    retorno = [listarq, int(porta)]
    return retorno

print("IP é localhost.")
porta = input(print("Digite porta: "))
server = SimpleXMLRPCServer(("localhost", int(porta)))
print("Escutando na porta: "+porta)

pasta = "./pasta"+porta+"/"

if os.path.isdir(pasta):
    print("Pasta ja existente.")
else:
    os.mkdir(pasta)
    print("Pasta criada com sucesso")

server.register_function(le_arquivo, "le_arq")
server.register_function(cria_arquivo, "cria_arq")
server.register_function(modifica_arquivo, "modifica_arq")
server.register_function(apaga_arquivo, "apaga_arq")
server.register_function(lista_arquivo, "lista_arq")
server.serve_forever()
