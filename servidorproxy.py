#Sistemas Distribuídos - 2017.2
#Projeto Final de SD - Sistema de Arquivos
#Gabriel da Rocha Silva - 374927

from xmlrpc.server import SimpleXMLRPCServer
import xmlrpc.client

def log(oper, nome_arq, porta_dn, respo):
    porta_dn = str(porta_dn)
    texto = "[ Operação: "+oper+" ][ Arquivo: "+nome_arq+" ][ Datanode: "+porta_dn+" ][ Resposta: "+respo+" ]\n"
    arquivo = open('logproxy.txt', 'r')
    conteudo = arquivo.readlines()
    conteudo.append(texto)
    arquivo = open('logproxy.txt', 'w')
    arquivo.writelines(conteudo)
    arquivo.close()
    return texto

def envialog():
    arq = open('logproxy.txt', 'r')
    arc = arq.read()
    arq.close()
    return arc


def ler(nome):
    try:
        porta = proxy.acha_arq(nome)
        if porta == "Nao":
            resposta = log("Ler", nome, porta, "Nao existe arquivo") #LOG
            lista = [resposta, ""]
            return lista
        else:
            try:
                retorno = xmlrpc.client.ServerProxy("http://localhost:" + str(porta) + "/").le_arq(nome)
                resposta = log("Ler", nome, porta, "ok") #LOG
                lista = [resposta, retorno]
                return lista
            except IOError:
                resposta = log("Ler", nome, porta, "Servidor desconectado") #LOG
                lista = [resposta, ""]
                return lista
    except IOError:
        resposta = log("Ler", nome, '', "Namenode falhou") #LOG
        lista = [resposta, ""]
        return lista


def criar(nome, texto):
    try:
        porta = proxy.cria_arq(nome)
        if porta == "Sim":
            resposta = log("Criar", nome, porta, "Arquivo ja existe") #LOG
            return resposta
        else:
            try:
                retorno = xmlrpc.client.ServerProxy("http://localhost:" + str(porta) + "/").cria_arq(nome, texto)
                resposta = log("Criar", nome, porta, "ok")  # LOG
                return resposta
            except IOError:
                proxy.apaga_arq(nome)
                resposta = log("Criar", nome, porta, "Servidor desconectado") #LOG
                return resposta
    except IOError:
        resposta = log("Criar", nome, '', "Namenode falhou") #LOG
        return resposta


def modificar(nome, texto):
    try:
        porta = proxy.acha_arq(nome)
        if porta == "Nao":
            resposta = log("Modificar", nome, porta, "Nao existe arquivo")  # LOG
            return resposta
        else:
            try:
                retorno = xmlrpc.client.ServerProxy("http://localhost:" + str(porta) + "/").modifica_arq(nome, texto)
                resposta = log("Modificar", nome, porta, "ok")  # LOG
                return resposta
            except IOError:
                resposta = log("Modificar", nome, porta, "Servidor desconectado")  # LOG
                return resposta
    except IOError:
        resposta = log("Modificar", nome, '', "Namenode falhou")  # LOG
        return resposta


def apagar(nome):
    try:
        porta = proxy.apaga_arq(nome)
        if porta == "Nao":
            resposta = log("Apagar", nome, porta, "Nao existe arquivo")  # LOG
            return resposta
        else:
            try:
                a = xmlrpc.client.ServerProxy("http://localhost:" + str(porta) + "/").apaga_arq(nome)
                resposta = log("Apagar", nome, porta, "ok")  # LOG
                return resposta
            except IOError:
                resposta = log("Apagar", nome, porta, "Servidor desconectado")  # LOG
                return resposta
    except IOError:
        resposta = log("Apagar", nome, '', "Namenode falhou")  # LOG
        return resposta

arq = open('logproxy.txt', 'w')
arq.write("")
arq.close()

print("Servidor Proxy")
server = SimpleXMLRPCServer(("localhost", 8000))
print("Listening on port 8000...")

print("Conectando ao namenode.")
proxy = xmlrpc.client.ServerProxy("http://localhost:7000/")
print("Conectado.")

server.register_function(ler, "ler")
server.register_function(criar, "criar")
server.register_function(modificar, "modificar")
server.register_function(apagar, "apagar")
server.register_function(envialog, "envialog")

server.serve_forever()