#Sistemas Distribuídos - 2017.2
#Projeto Final de SD - Sistema de Arquivos
#Gabriel da Rocha Silva - 374927

from xmlrpc.server import SimpleXMLRPCServer
import xmlrpc.client
import hashlib

def acha_arq(nome):
    nome = nome + ".txt"
    if nome in dic:
        return dic[nome]
    else:
        return "Nao"

def cria_arq(nome):
    nome = nome + ".txt"
    if nome in dic:
        return "Sim"
    else:
        value = nome.encode('utf-8')
        h = hashlib.sha256(value)
        h.hexdigest()
        numero = int(h.hexdigest(), base=16)
        dic[nome] = listadn[numero%num]
        return int(listadn[numero%num])

def apaga_arq(nome):
    nome = nome + ".txt"
    if nome in dic:
        servidor = dic[nome]
        del(dic[nome])
        return servidor
    else:
        return "Nao"

print("Bem-vindo ao namenode.")
num = int(input(print("Insira numero de datanodes: ")))
i = 0
listadn = []
dic = {}

while i < num:
    portadn = input(print("Digite porta Datanode: "))
    listadn.append(portadn)
    i += 1


for i in range(0, num, 1):
    proxy = xmlrpc.client.ServerProxy("http://localhost:"+listadn[i]+"/")
    retorno = proxy.lista_arq()
    tamanho = len(retorno[0])
    for w in range(0, tamanho, 1):
        porta = retorno[1]
        nome = retorno[0][w]
        dic[nome] = porta


server = SimpleXMLRPCServer(("localhost", 7000))
print("Escutando na porta 7000...")

server.register_function(acha_arq, "acha_arq")
server.register_function(cria_arq, "cria_arq")
server.register_function(apaga_arq, "apaga_arq")
server.serve_forever()